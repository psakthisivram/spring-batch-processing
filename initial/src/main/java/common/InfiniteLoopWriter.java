package common;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.item.ItemWriter;

/**
* Simple module implementation that will always return true to indicate that
* processing should continue. This is useful for testing graceful shutdown of
* jobs.
* 
* @author Lucas Ward
* 
*/
public class InfiniteLoopWriter extends StepExecutionListenerSupport implements ItemWriter<Object> {
	private static final Log LOG = LogFactory.getLog(InfiniteLoopWriter.class);

	private StepExecution stepExecution;
	private int count = 0;

	/**
	 * @see org.springframework.batch.core.StepExecutionListener#beforeStep(StepExecution)
	 */
	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	public InfiniteLoopWriter() {
		super();
	}

	@Override
	public void write(List<? extends Object> items) throws Exception {
		/*try {
			Thread.sleep(500);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new IllegalStateException("Job interrupted.", e);
		}*/

		stepExecution.setWriteCount(++count);
		//LOG.info("Executing infinite loop, at count=" + count);
		//LOG.info("Writing : "+items);
	}
}