package common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfiniteApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(InfiniteApplication.class, args);
    }
}