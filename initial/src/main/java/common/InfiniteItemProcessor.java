package common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

public class InfiniteItemProcessor implements ItemProcessor<Integer, Integer> {

	private static final Logger log = LoggerFactory.getLogger(InfiniteItemProcessor.class);

	@Override
	public Integer process(final Integer item) throws Exception {

		//if(item > 25000) return null;
		//log.info("Inside InfiniteItemProcessor ! : "+item);

		return item;
	}

}